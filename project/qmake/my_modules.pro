TEMPLATE = subdirs
CONFIG *= ordered

SUBDIRS *= \
    $${PWD}/../../my_modules/module-1/project/qmake/module-1-lib-1.pro \
    $${PWD}/../../my_modules/module-1/project/qmake/module-1-lib-2.pro \
    $${PWD}/../../my_modules/module-2/project/qmake/my_app.pro \
