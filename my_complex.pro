TEMPLATE = subdirs
CONFIG *= ordered

isEqual( QT_MAJOR_VERSION, 4 ) : \
    SUBDIRS *= \
        $${PWD}/project/qmake/features/pro/.qmake.qt4.pro \

# Опционально можно включить в проект
SUBDIRS *= \
    $${PWD}/project/qmake/features/pro/.depends.cache.pro \
    $${PWD}/project/qmake/features/pro/features.pro \

SUBDIRS *= \
    $${PWD}/project/qmake/my_modules.pro \

OTHER_FILES *= \
    $${PWD}/.qmake.conf \
